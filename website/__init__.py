from flask import Flask, jsonify, redirect, session, url_for, render_template, request
import stripe, json


def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = 'Compotas12@'

    from .views import views
    from .auth import auth

    app.register_blueprint(views, url_prefix='/')
    app.register_blueprint(auth, url_prefix='/')

    # Setup Stripe python client library
    # load_dotenv(find_dotenv())
    # For sample support and debugging, not required for production:
    stripe.set_app_info(
        'stripe-samples/checkout-single-subscription',
        version='0.0.1',
        url='https://github.com/stripe-samples/checkout-single-subscription')

    stripe.api_version = '2020-08-27'
    stripe.api_key = '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'

    # static_dir = str(os.path.abspath(os.path.join(
    #     __file__, "..", os.getenv("STATIC_DIR"))))
    # app = Flask(__name__, static_folder=static_dir,
    #             static_url_path="", template_folder=static_dir)

    # @app.route('/', methods=['GET'])
    # def get_example():
    #     return render_template('index.html')

    @app.route('/config', methods=['GET'])
    def get_publishable_key():
        return jsonify({
            'publishableKey': 'pk_live_51JxcojKK3D090enHRUYK5SZZXdLJMd2nMPjVZdWIgdpLdTT4FWMoIaTNCaudVh8FBoof1DhM7zBlTGk2FinLjjpJ00JaHT9KQ0',
            'basicPrice': 'price_1KDandKK3D090enHW1L5AG8l',
            'proPrice': 'price_1KBP8cKK3D090enHck8jDNGJ'
        })

    # Fetch the Checkout Session to display the JSON result on the success page
    @app.route('/checkout-session', methods=['GET'])
    def get_checkout_session():
        id = request.args.get('sessionId')
        checkout_session = stripe.checkout.Session.retrieve(id)
        return jsonify(checkout_session)

    @app.route('/create-checkout-session', methods=['POST'])
    def create_checkout_session():
        price = request.form.get('priceId')
        domain_url = 'http://127.0.0.1:5000/'

        try:
            # Create new Checkout Session for the order
            # Other optional params include:
            # [billing_address_collection] - to display billing address details on the page
            # [customer] - if you have an existing Stripe Customer ID
            # [customer_email] - lets you prefill the email input in the form
            # [automatic_tax] - to automatically calculate sales tax, VAT and GST in the checkout page
            # For full details see https://stripe.com/docs/api/checkout/sessions/create

            # ?session_id={CHECKOUT_SESSION_ID} means the redirect will have the session ID set as a query param
            checkout_session = stripe.checkout.Session.create(
                success_url=domain_url + '/success.html?session_id={CHECKOUT_SESSION_ID}',
                cancel_url=domain_url + '/canceled.html',
                mode='subscription',
                # automatic_tax={'enabled': True},
                line_items=[{
                    'price': price,
                    'quantity': 1
                }],
            )
            return redirect(checkout_session.url, code=303)
        except Exception as e:
            return jsonify({'error': {'message': str(e)}}), 400

    @app.route('/customer-portal', methods=['POST'])
    def customer_portal():
        # For demonstration purposes, we're using the Checkout session to retrieve the customer ID.
        # Typically this is stored alongside the authenticated user in your database.
        checkout_session_id = request.form.get('sessionId')
        checkout_session = stripe.checkout.Session.retrieve(checkout_session_id)

        # This is the URL to which the customer will be redirected after they are
        # done managing their billing with the portal.
        return_url = 'http://127.0.0.1:5000/'

        session = stripe.billing_portal.Session.create(
            customer=checkout_session.customer,
            return_url=return_url,
        )
        return redirect(session.url, code=303)

    # @app.route('/webhook', methods=['POST'])
    # def webhook_received():
    #     # You can use webhooks to receive information about asynchronous payment events.
    #     # For more about our webhook events check out https://stripe.com/docs/webhooks.
    #     webhook_secret = 'whsec_84iLGdVvKwleObejsCLoLcUZIyvwp8rd'
    #     request_data = json.loads(request.data)
    #
    #     if webhook_secret:
    #         # Retrieve the event by verifying the signature using the raw body and secret if webhook signing is configured.
    #         signature = request.headers.get('stripe-signature')
    #         try:
    #             event = stripe.Webhook.construct_event(
    #                 payload=request.data, sig_header=signature, secret=webhook_secret)
    #             data = event['data']
    #         except Exception as e:
    #             return e
    #         # Get the type of webhook event sent - used to check the status of PaymentIntents.
    #         event_type = event['type']
    #     else:
    #         data = request_data['data']
    #         event_type = request_data['type']
    #     data_object = data['object']
    #
    #     print('event ' + event_type)
    #
    #     if event_type == 'checkout.session.completed':
    #         print('🔔 Payment succeeded!')
    #
    #     return jsonify({'status': 'success'})

    # oauth = OAuth(app)
    # auth0 = oauth.register(
    #     'auth0',
    #     client_id='FHkTJJBqokmI3AxaTfBbS3NEC7eHO2nv',
    #     client_secret='x1bh_64L44NyGy4PUoYMlFmDantuz1HlAHg1GdzEZ9xo37WB4ZcUVVWucRZqBcyY',
    #     api_base_url='https://shiny.legalcyclopedia.com',
    #     access_token_url='https://shiny.legalcyclopedia.com/’',
    #     authorize_url='https://shiny.legalcyclopedia.com/authorize',
    #     client_kwargs={
    #         'scope': 'openid email profile read:users read:user_idp_tokens read:current_user_metadata create:current_user_metadatal',
    #     },
    # )

    # @app.route('/callback')
    # def callback_handling():
    #     # Handles response from token endpoint
    #     auth0.authorize_access_token()
    #     resp = auth0.get('userinfo')
    #     userinfo = resp.json()
    #
    #     # Store the user information in flask session.
    #     session['jwt_payload'] = userinfo
    #     session['profile'] = {
    #         'user_id': userinfo['sub'],
    #         'name': userinfo['name'],
    #         'picture': userinfo['picture']
    #     }
    #     return redirect('/dashboard')
    #
    # @app.route('/login-auth0')
    # def login_auth0():
    #     return auth0.authorize_redirect(redirect_uri='https://shiny.legalcyclopedia.com/callback')
    #
    # def requires_auth(f):
    #     @wraps(f)
    #     def decorated(*args, **kwargs):
    #         if 'profile' not in session:
    #             # Redirect to Login page here
    #             return redirect('/')
    #         return f(*args, **kwargs)
    #
    #     return decorated
    #
    # @app.route('/dashboard')
    # @requires_auth
    # def dashboard():
    #     return render_template('dashboard.html',
    #                            userinfo=session['profile'],
    #                            userinfo_pretty=json.dumps(session['jwt_payload'], indent=4))
    #
    # @app.route('/logout')
    # def logout():
    #     # Clear session stored data
    #     session.clear()
    #     # Redirect user to logout endpoint
    #     params = {'returnTo': url_for('home', _external=True), 'client_id': 'FHkTJJBqokmI3AxaTfBbS3NEC7eHO2nv'}
    #     return redirect(auth0.api_base_url + '/v2/logout?' + urlencode(params))

    return app


