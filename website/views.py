from flask import Blueprint, render_template, redirect

views = Blueprint('views', __name__)

@views.route('/')
@views.route('/home')
def home():
    return render_template('index.html')

@views.route('/pricing')
def pricing():
    return render_template('pricing.html')

@views.route('/about-us')
def about_us():
    return render_template('about-us.html')

@views.route('/login')
def login():
    return render_template('login.html')

@views.route('/features')
def features():
    return redirect("www.google.com")

@views.route('/checkout')
def checkout():
    return render_template('checkout.html')

